//! Simple games implementations
//!
//! ## Available games:
//!
//! - [Hangman](hangman/index.html)
//!
//! ## Features:
//!
//! - `std` - Enables usage of standard library. Without it requires `alloc` module. Enabled by default.

#![warn(missing_docs)]
#![cfg_attr(feature = "cargo-clippy", allow(clippy::style))]

#[cfg(feature = "std")]
use std as alloc;
#[cfg(not(feature = "std"))]
extern crate alloc;

pub mod level;
pub mod hangman;

pub use level::Level;
pub use hangman::Hangman;
