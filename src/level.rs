//!Utility struct to provide leveling facilities.

use core::{cmp, fmt};
use core::marker::PhantomData;

///Describes configuration for leveling system
pub trait LevelConfig {
    ///Max level
    const MAX_LEVEL: u8;
    ///Max experience
    const MAX_EXP: u64;

    ///Describes algorithm of conversion of experience to level.
    ///
    ///Constraints:
    ///
    ///- `exp` can be within range from 0 to `MAX_EXP`
    fn exp_to_level(exp: u64) -> u8;

    ///Describes algorithm of conversion of level to required experience.
    ///
    ///Constraints:
    ///
    ///- `level` can be within range from 0 to `MAX_LEVEL`
    fn level_to_exp(level: u8) -> u64;
}

///Describes result of adding experience
#[derive(Debug, PartialEq)]
pub enum DecreaseResult {
    ///Removed experience.
    Decreased,
    ///Level down happened.
    LevelDown,
}

///Describes result of adding experience
#[derive(Debug, PartialEq)]
pub enum AddResult {
    ///Max Level is reached
    Maxed,
    ///Level up happened.
    LevelUp,
    ///Added experience.
    Added,
}

///Describers experience points and level.
pub struct Level<C> {
    ///Current experience points
    exp: u64,
    ///Current level.
    level: u8,
    _config: PhantomData<C>
}

impl<C: LevelConfig> Level<C> {
    ///Creates new instance with initial experience.
    pub fn with_exp(mut exp: u64) -> Self {
        exp = cmp::min(C::MAX_EXP, exp);
        let level = C::exp_to_level(exp);

        Self {
            exp,
            level,
            _config: PhantomData
        }
    }

    #[inline(always)]
    ///Returns current experience
    pub fn current_exp(&self) -> u64 {
        self.exp
    }

    #[inline(always)]
    ///Returns current level
    pub fn current_level(&self) -> u8 {
        self.level
    }

    ///Changes config of level up system while retaining existing experience.
    #[inline(always)]
    pub fn map_config<N: LevelConfig>(self, _: N) -> Level<N> {
        Level {
            exp: self.exp,
            level: self.level,
            _config: PhantomData
        }
    }

    ///Decrease experience points.
    pub fn decrease(&mut self, val: u64) -> DecreaseResult {
        self.exp = self.exp.saturating_sub(val);
        let new_level = C::exp_to_level(self.exp);

        match new_level != self.level {
            true => {
                self.level = new_level;
                DecreaseResult::LevelDown
            },
            false => DecreaseResult::Decreased
        }
    }

    ///Adds experience points and returns
    ///whether level up happened.
    pub fn add(&mut self, val: u64) -> AddResult {
        if self.level >= C::MAX_LEVEL {
            return AddResult::Maxed;
        }

        self.exp = cmp::min(C::MAX_EXP, self.exp + val);
        let new_level = C::exp_to_level(self.exp);

        if new_level != self.level {
            self.level = new_level;
            AddResult::LevelUp
        } else {
            AddResult::Added
        }
    }
}

impl<C: LevelConfig> fmt::Display for Level<C> {
    fn fmt(&self, w: &mut fmt::Formatter) -> fmt::Result {
        match self.level >= C::MAX_LEVEL {
            true => write!(w, "{}", C::MAX_EXP),
            false => {
                let next_level = self.level as u8 + 1;
                let exp_until_next = C::level_to_exp(next_level);
                write!(w, "{}/{}", self.exp, exp_until_next)
            }
        }
    }
}
