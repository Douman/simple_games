use simple_games::level::{Level, DecreaseResult, AddResult, LevelConfig};

use core::cmp;

struct Config;

impl LevelConfig for Config {
    const MAX_LEVEL: u8 = 10;
    const MAX_EXP: u64 = 55 * 10 * 10 * 10;

    fn exp_to_level(exp: u64) -> u8 {
        let exp = cmp::min(exp, Self::MAX_EXP);

        let exp = (exp / 55) as f32;
        let exp = exp.powf(1.0 / 3.0);
        exp as u8
    }

    fn level_to_exp(level: u8) -> u64 {
        let level = level as u64;
        55 * level * level * level
    }
}

#[test]
fn should_level_up() {
    let mut level = Level::<Config>::with_exp(0);
    assert_eq!(level.current_level(), 0);

    assert_eq!(level.decrease(550), DecreaseResult::Decreased);
    assert_eq!(level.current_exp(), 0);

    assert_eq!(level.add(550), AddResult::LevelUp);
    assert_eq!(level.current_level(), 2);

    assert_eq!(level.add(0), AddResult::Added);
    assert_eq!(level.current_level(), 2);
    assert_eq!(level.add(1), AddResult::Added);
    assert_eq!(level.current_level(), 2);
    assert_eq!(level.add(10), AddResult::Added);
    assert_eq!(level.current_level(), 2);
    let add_exp = 1_485 - level.current_exp() - 1;
    assert_eq!(level.add(add_exp), AddResult::Added);
    assert_eq!(level.current_level(), 2);
    assert_eq!(level.add(1), AddResult::LevelUp);
    assert_eq!(level.current_exp(), 1_485);
    println!("{}", level);
    assert_eq!(level.current_level(), 3);
    assert_eq!(level.add(Config::MAX_EXP), AddResult::LevelUp);
    assert_eq!(level.current_exp(), Config::MAX_EXP);
    assert_eq!(level.current_level(), 10);
    println!("{}", level);

    assert_eq!(level.decrease(Config::MAX_EXP), DecreaseResult::LevelDown);
    assert_eq!(level.current_exp(), 0);
    assert_eq!(level.current_level(), 0);
}
