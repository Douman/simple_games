//! Hangman game module
//!
//! One player thinks of a word, phrase or sentence and the other(s) tries to guess it by
//! suggesting letters or numbers, within a certain number of guesses.
//!
//! # Usage
//!
//! ```rust
//! use simple_games::{hangman, Hangman};
//!
//! let mut game = Hangman::new("loli".into(), 5);
//! let result = game.guess_letter('l');
//! assert!(!result.is_over());
//!
//! match result {
//!     hangman::GuessResult::Mada(word, rem_att, true) => {
//!         println!("Guessed letter 'l', remaining attempts: {}", rem_att);
//!         println!("Current word: {}", word);
//!     },
//!     _ => panic!("Unexpected result of guess, should partly guess.")
//! }
//! ```

use crate::alloc::vec::Vec;
use crate::alloc::string::String;
use crate::alloc::borrow::Cow;

#[derive(Debug, PartialEq, Clone)]
///Result of guessing
pub enum GuessResult {
    ///Failed to guess within number of turns.
    GameOver(String),
    ///Successfully guessed the word.
    Victory(String),
    ///Not finished yet
    ///
    ///# Args
    ///
    ///- Current word;
    ///- Left attempts;
    ///- Did you guessed some letter?;
    Mada(String, usize, bool),
}

impl GuessResult {
    ///Returns whether game is finished.
    pub fn is_over(&self) -> bool {
        match self {
            GuessResult::GameOver(_) | GuessResult::Victory(_) => true,
            _ => false,
        }
    }
}

///Hangman game state
pub struct Hangman<'a> {
    word: Cow<'a, str>,
    current: Vec<char>,
    attempts: usize,
    max: usize,
}

impl<'a> Hangman<'a> {
    ///Creates new game with provided word, and max number of guesses.
    pub fn new(word: Cow<'a, str>, max: usize) -> Self {
        let char_len = word.chars().count();
        let mut current = Vec::with_capacity(char_len);
        current.resize(char_len, '_');

        Self {
            word,
            current,
            attempts: 0,
            max,
        }
    }

    #[inline]
    ///Returns number of already made  guesses
    pub fn attempts(&self) -> usize {
        self.attempts
    }

    #[inline]
    ///Returns current state of the word as slice of characters.
    ///
    ///It's size is equal to `String::chars().count()` of original word
    pub fn current_state(&self) -> &[char] {
        &self.current
    }

    ///Retrieves the current state of word. With unknown letters replaced by `_` and each letter
    ///separated by white space
    pub fn current_word(&self) -> String {
        let mut result = String::with_capacity(self.word.len() * 2);

        for idx in 0..self.current.len()-1 {
            result.push(self.current[idx]);
            result.push(' ');
        }
        result.push(self.current[self.current.len()-1]);

        result
    }

    #[inline]
    ///Returns whether game is solved
    pub fn is_game_solved(&self) -> bool {
        !self.current.iter().any(|ch| *ch == '_')
    }

    ///Attempts to guess letter.
    ///
    ///See [GuessResult](enum.GuessResult.html) for details
    pub fn guess_letter(&mut self, letter: char) -> GuessResult {
        if self.attempts >= self.max {
            return GuessResult::GameOver(self.current_word());
        }

        let mut result = false;

        for (idx, ch) in self.word.chars().enumerate() {
            if self.current[idx] == '_' && ch == letter {
                self.current[idx] = letter;
                result = true;
            }
        }

        self.attempts += 1;

        if self.is_game_solved() {
            GuessResult::Victory(self.word.to_string())
        } else if self.attempts < self.max {
            GuessResult::Mada(self.current_word(), self.max - self.attempts, result)
        } else {
            GuessResult::GameOver(self.current_word())
        }
    }
}
