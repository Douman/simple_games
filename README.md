# simple_games

[![Build](https://gitlab.com/Douman/simple_games/badges/master/build.svg)](https://gitlab.com/Douman/simple_games/pipelines)
[![Crates.io](https://img.shields.io/crates/v/simple_games.svg)](https://crates.io/crates/simple_games)
[![Documentation](https://docs.rs/simple_games/badge.svg)](https://docs.rs/crate/simple_games/)

Implementation of simple games

## Available games

- Hangman;

## Features

- `std` - Enables usage of standard library. Without it requires `alloc` module. Enabled by default.
