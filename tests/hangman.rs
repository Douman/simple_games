use simple_games::{hangman, Hangman};

#[test]
fn hangman_works() {
    let mut game = Hangman::new("loli".into(), 5);

    assert_eq!(game.current_word(), "_ _ _ _");

    let result = game.guess_letter('l');
    assert!(!result.is_over());
    match result {
        hangman::GuessResult::Mada(word, rem_att, is_guessed) => {
            assert_eq!(word, "l _ l _");
            assert_eq!(rem_att, 4);
            assert!(is_guessed);
        },
        _ => panic!("Unexpected result of guess, should partly guess.")
    }

    let result = game.guess_letter('l');
    assert!(!result.is_over());
    match result {
        hangman::GuessResult::Mada(word, rem_att, is_guessed) => {
            assert_eq!(word, "l _ l _");
            assert_eq!(rem_att, 3);
            assert!(!is_guessed);
        },
        _ => panic!("Unexpected result of guess, should partly guess.")
    }

    let result = game.guess_letter('o');
    assert!(!result.is_over());
    match result {
        hangman::GuessResult::Mada(word, rem_att, is_guessed) => {
            assert_eq!(word, "l o l _");
            assert_eq!(rem_att, 2);
            assert!(is_guessed);
        },
        _ => panic!("Unexpected result of guess, should partly guess.")
    }

    let result = game.guess_letter('i');
    assert!(result.is_over());
    match result {
        hangman::GuessResult::Victory(word) => {
            assert_eq!(word, "loli");
        },
        _ => panic!("Unexpected result of guess, should win guess.")
    }
}

#[test]
fn should_get_game_over() {
    let mut game = Hangman::new("loli".into(), 2);

    assert_eq!(game.current_word(), "_ _ _ _");

    let result = game.guess_letter('l');
    assert!(!result.is_over());
    match result {
        hangman::GuessResult::Mada(word, rem_att, is_guessed) => {
            assert_eq!(word, "l _ l _");
            assert_eq!(rem_att, 1);
            assert!(is_guessed);
        },
        _ => panic!("Unexpected result of guess, should partly guess.")
    }

    let result = game.guess_letter('l');
    assert!(result.is_over());
    match result {
        hangman::GuessResult::GameOver(word) => {
            assert_eq!(word, "l _ l _");
        },
        _ => panic!("Unexpected result of guess, should game over.")
    }

    let result = game.guess_letter('l');
    assert!(result.is_over());
    match result {
        hangman::GuessResult::GameOver(word) => {
            assert_eq!(word, "l _ l _");
        },
        _ => panic!("Unexpected result of guess, should game over.")
    }

}
